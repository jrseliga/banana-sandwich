#!/bin/sh

# terminate running instance
killall -q polybar

while pgrep -u $USER -x polybar >/dev/null; do sleep 1; done
(sleep 3; polybar bar) &

# For some reason when BSWPM is restarted polybar is displayed
# below the gnome-fallback wallpaper. This is a quick fix.
gsettings set org.gnome.gnome-flashback desktop false
gsettings set org.gnome.gnome-flashback desktop true
