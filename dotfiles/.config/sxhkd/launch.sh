#!/bin/sh

# terminate running instance
killall -q sxhkd

while pgrep -u $USER -x sxhkd >/dev/null; do sleep 1; done

# -m -1 seem to be necessary for chords to work at startup
sxhkd -m -1 -a Return &

# ensure always running
#while :; do
#    if pgrep -x sxhkd > /dev/null; then
#        true
#    else
#        # -m -1 seem to be necessary for chords to work at startup
#        sxhkd -m -1 -a Return &
#    fi
#    sleep 5
#done &
