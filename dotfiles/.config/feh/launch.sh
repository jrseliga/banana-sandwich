#!/bin/sh

# terminate running instance
killall -q feh

# ensure always running
while :; do
    if pgrep -x feh > /dev/null; then
        true
    else
        feh --bg-fill ~/.background.png &
    fi
    sleep 5
done &
