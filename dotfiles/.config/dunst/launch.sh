#!/bin/sh

# terminate running instance
killall -q dunst

# ensure always running
while :; do
    if pgrep -x dunst > /dev/null; then
        true
    else
        dunst -geom "280x50-10+38" -frame_width "1" &
    fi
    sleep 5
done &