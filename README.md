# Installation
> `git clone desktop`
> `git clone operating-systems` as sibling to `desktop`
> `composer install` (desktop and operating-systems)
> `sh setup.sh`
> `sh install.sh`
> `php desktop install -y`

# Inspirational / Helpful repositories
- bspwm-gnome
- nord_for_windows_by_niivu_dexqahn
- prdanellii-dotfiles
- Genome (Nebula)
- bspwm-ubuntu
- Debian-titus
