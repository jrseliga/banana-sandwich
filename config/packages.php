<?php

use App\Commands\Neofetch;

return [
    'commands' => [
        Neofetch\Install::class => [
            'apt' => [
                'neofetch'
            ],
        ],
    ],
];