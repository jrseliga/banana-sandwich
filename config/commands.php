<?php

use OperatingSystems\Commands\Volume;
use OperatingSystems\Commands\Release;
use OperatingSystems\Commands\Packages;
use OperatingSystems\Commands\Brightness;
use OperatingSystems\Commands\DateTime\DateTime;
use OperatingSystems\Commands\Terminal;
use OperatingSystems\Packages\Apt\Console\Commands as Apt;
use OperatingSystems\Packages\Dnf\Console\Commands as Dnf;
use OperatingSystems\Packages\Git\Console\Commands as Git;
use OperatingSystems\Packages\Rofi\Console\Commands as Rofi;
use OperatingSystems\Packages\Snap\Console\Commands as Snap;
use OperatingSystems\Packages\AlsaUtils\Console\Commands\Amixer;
use OperatingSystems\Packages\Flatpak\Console\Commands as Flatpak;
use OperatingSystems\Packages\Alacritty\Console\Commands\Alacritty;
use OperatingSystems\Packages\Ulauncher\Console\Commands\Ulauncher;
use OperatingSystems\Packages\AptCache\Console\Commands as AptCache;
use OperatingSystems\Packages\LibNotify\Console\Commands as LibNotify;
use OperatingSystems\Packages\BrightnessCtl\Console\Commands as BrightnessCtl;
use OperatingSystems\Packages\Bspwm\Console\Commands\Bspc;
use OperatingSystems\Packages\LinuxStandardBase\Console\Commands\LinuxStandardBase;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Command
    |--------------------------------------------------------------------------
    |
    | Laravel Zero will always run the command specified below when no command name is
    | provided. Consider update the default command for single command applications.
    | You cannot pass arguments to the default command because they are ignored.
    |
    */

    'default' => NunoMaduro\LaravelConsoleSummary\SummaryCommand::class,

    /*
    |--------------------------------------------------------------------------
    | Commands Paths
    |--------------------------------------------------------------------------
    |
    | This value determines the "paths" that should be loaded by the console's
    | kernel. Foreach "path" present on the array provided below the kernel
    | will extract all "Illuminate\Console\Command" based class commands.
    |
    */

    'paths' => [app_path('Commands')],

    /*
    |--------------------------------------------------------------------------
    | Added Commands
    |--------------------------------------------------------------------------
    |
    | You may want to include a single command class without having to load an
    | entire folder. Here you can specify which commands should be added to
    | your list of commands. The console's kernel will try to load them.
    |
    */

    'add' => [
        DateTime::class,
        LinuxStandardBase::class,
        Dnf\Install::class,
        Dnf\Clean::class,
        Dnf\AutoRemove::class,
        Dnf\MakeCache::class,
        Dnf\Update::class,
        Apt\Install::class,
        Apt\Upgrade::class,
        Apt\AutoRemove::class,
        Apt\AutoClean::class,
        AptCache\Policy::class,
        Packages\Make::class,
        Packages\Candidates\Make::class,
        LibNotify\NotifySend::class,
        Release::class,
        Terminal::class,
        Rofi\Rofi::class,
        Bspc\Query::class,
        Bspc\Desktop::class,
        Bspc\Node::class,
        Alacritty::class,
        Rofi\Rofi\Window::class,
        Rofi\Rofi\Drun::class,
        Ulauncher\Toggle::class,
        Brightness\Get::class,
        Brightness\Up::class,
        Brightness\Down::class,
        Volume\Get::class,
        Volume\Mute::class,
        Volume\Unmute::class,
        Volume\Up::class,
        Volume\Down::class,
        Volume\Toggle::class,
        Amixer\Get::class,
        Amixer\Mute::class,
        Amixer\Unmute::class,
        Amixer\Up::class,
        Amixer\Down::class,
        Amixer\Toggle::class,
        BrightnessCtl\Get::class,
        BrightnessCtl\Max::class,
        BrightnessCtl\Up::class,
        BrightnessCtl\Down::class,
        Git\Replicate::class,
        Git\Pull::class,
        Git\Fetch::class,
        Snap\Install::class,
        Snap\Installed::class,
        Snap\Remove::class,
        Snap\Refresh::class,
        Flatpak\Install::class,
        Flatpak\Update::class,
        Flatpak\Repository\Add::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Hidden Commands
    |--------------------------------------------------------------------------
    |
    | Your application commands will always be visible on the application list
    | of commands. But you can still make them "hidden" specifying an array
    | of commands below. All "hidden" commands can still be run/executed.
    |
    */

    'hidden' => [
        // TODO: Capture root does not need to be a visible command, using for testing, uncomment later
        // CaptureRoot::class,
        NunoMaduro\LaravelConsoleSummary\SummaryCommand::class,
        Symfony\Component\Console\Command\HelpCommand::class,
        Illuminate\Console\Scheduling\ScheduleRunCommand::class,
        Illuminate\Console\Scheduling\ScheduleFinishCommand::class,
        Illuminate\Foundation\Console\VendorPublishCommand::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Removed Commands
    |--------------------------------------------------------------------------
    |
    | Do you have a service provider that loads a list of commands that
    | you don't need? No problem. Laravel Zero allows you to specify
    | below a list of commands that you don't to see in your app.
    |
    */

    'remove' => [
        // ..
    ],

];
