<?php

namespace App\Commands;

use Symfony\Component\Process\Process;
use LaravelZero\Framework\Commands\Command;
use OperatingSystems\Facades\OperatingSystem;
use Symfony\Component\Process\ExecutableFinder;

class CaptureRoot extends Command
{
    protected $signature = 'root:capture';

    protected $description = 'Capture root privileges.';

    public function handle()
    {
        (new Process(['sudo', 'ls', '-la']))->setTty(false)->run();
    }
}
