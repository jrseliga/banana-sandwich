<?php

namespace App\Commands\Ssh\Key;

use App\Commands\Concerns\PublishesFiles;
use Illuminate\Support\Facades\File;
use LaravelZero\Framework\Commands\Command;

class Generate extends Command
{
    use PublishesFiles;

    protected $signature = 'ssh:key:generate';

    protected $description = 'Generate your SSH key.';

    public function handle()
    {
        $this->task('Generate SSH Key', function () {
            $this->generateKey();
        });
    }

    protected function capturePassphrase(): string
    {
        $passphrase = $this->secret('Enter passphrase (empty for no passphrase)');
        $confirmation = $this->secret('Enter same passphrase again');

        return $confirmation === $passphrase ? $passphrase : $this->mismatchedPassphrase();

    }

    protected function mismatchedPassphrase(): string
    {
        $this->error('Passphrases did not match, try again:');

        return $this->capturePassphrase();
    }

    protected function generateKey(): void
    {
        if ($this->keyExists()) {
            return;
        }


        shell_exec(
            sprintf(
                'ssh-keygen -t rsa -b 4096 -P %s -f %s',
                $this->capturePassphrase(),
                $this->path()
            ),
        );
    }

    protected function keyExists(): bool
    {
        return File::exists(
            $this->path()
        );
    }

    protected function path(): string
    {
        return home_path('.ssh/id_rsa');
    }
}
