<?php

namespace App\Commands\Wallpaper;

use App\Commands\Concerns\PublishesFiles;
use LaravelZero\Framework\Commands\Command;

class Set extends Command
{
    use PublishesFiles;

    protected $signature = 'wallpaper:set';

    protected $description = 'Set the theme wallpaper';

    public function handle()
    {
        $this->task('Set wallpaper', function () {
            $this->publish();
        });
    }
    
    protected function directories(): array
    {
        return [
            home_path('Photos/wallpapers')
        ];
    }

    protected function links(): array
    {
        // collect(File::allFiles(base_path('assets/wallpapers')))->dd();
    }
}
