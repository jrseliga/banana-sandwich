<?php

namespace App\Commands;

use App\Commands\Ssh;
use App\Commands\Github;
use App\Commands\Gitlab;
use App\Commands\Packages;
use App\Commands\Os;
use App\Commands\Packages\Sanitize;
use LaravelZero\Framework\Commands\Command;

class Update extends Command
{
    protected $signature = 'update {--y|yes}';

    protected $description = 'Update your desktop.';

    public function handle()
    {
        if (! $this->confirmed()) {
            return;
        }

        $this->captureRoot();
        $this->runOperatingSystemTasks();
    }

    protected function confirmed(): bool
    {
        return $this->option('yes') || $this->confirm('Root privileges are required to install packages, would you like to continue?');
    }

    protected function captureRoot(): void
    {
        $this->call(CaptureRoot::class);
    }

    protected function runOperatingSystemTasks(): void
    {
        $this->title('Operating System');

        $this->call(Os\Update::class);
    }
}
