<?php

namespace App\Commands\Packages;

use OperatingSystems\Commands\Snap;
use LaravelZero\Framework\Commands\Command;

class Sanitize extends Command
{
    protected $signature = 'packages:sanitize';

    protected $description = 'Sanitize packages pre-installed by operating system.';

    public function handle()
    {
        $this->call(Snap\Remove::class, ['packages' => ['firefox', 'snap-store'], '--purge' => true]);
    }
}
