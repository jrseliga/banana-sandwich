#!/bin/sh

install () {
    installTools
    installBase
    installAppearance
}

installTools () {
    sudo apt install --assume-yes \
        python3-pip \
        php \
        php-xml \
        composer
}

installBase () {
        bash -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
        git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
}

installAppearance () {
    installAppearanceThemes
}

installAppearanceThemes () {
    if [ -d "$HOME/.themes/Nordic" ]
    then
	echo 'skipping'
        # git fetch -C "$HOME/.themes/Nordic"
        # git pull --rebase -C "$HOME/.themes/Nordic"
    else
        git clone https://github.com/EliverLara/Nordic.git "$HOME/.themes/Nordic"
    fi

    if [ -d "$HOME/.config/ulauncher/user-themes/nord" ]
    then
	echo 'skipping'
        # git fetch -C "$HOME/.config/ulauncher/user-themes/nord"
        # git pull --rebase -C "$HOME/.config/ulauncher/user-themes/nord"
    else
        git clone https://github.com/KiranWells/ulauncher-nord.git "$HOME/.config/ulauncher/user-themes/nord"
    fi

    if [ -d "$HOME/.config/alacritty/themes/nord" ]
    then
	echo 'skipping'
        # git fetch -C "$HOME/.config/alacritty/themes/nord"
        # git pull --rebase -C "$HOME/.config/alacritty/themes/nord"
    else
        git clone https://github.com/arcticicestudio/nord-alacritty.git "$HOME/.config/alacritty/themes/nord"
    fi

    cp "$HOME/.config/alacritty/themes/nord/src/nord.yml" "$HOME/.config/alacritty/alacritty.yml"

    cp "$HOME/.config/neofetch/themes/nord.conf" "$HOME/.config/neofetch/config.conf"

    gsettings set org.gnome.desktop.interface gtk-theme "Nordic"
    gsettings set org.gnome.desktop.wm.preferences theme "Nordic"
    sudo flatpak override --filesystem=$HOME/.themes
    sudo flatpak override --env=GTK_THEME=Nordic
    sudo flatpak override --env=XCURSOR_THEME=Breeze_Snow
}


install
